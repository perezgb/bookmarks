# Bookmarks

![img](/diagrams/bookmarks.drawio.png)

## Project Structure
For learning purposes this project has two AWS setups. One using vanilla Cloudformation and one using the Serverless Framework. They are under the `cloudformation-plain` and `serverless` folders respectively.

- cloudformation-plain
- diagrams
- frontend
- sample-data
- scripts

## AWS Setup
Install the AWS CLI using [brew](https://formulae.brew.sh/formula/awscli):
```
brew install awscli
```
Or by following the [docs](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html).

Configure AWS credentials:
```
aws configure
```

Ensure credentials are valid:
```
aws sts get-caller-identity
```

## Setup - Serverless
### Requirements
Ensure the serverless framework is installed:
```
sls --version
```

Or install it with the command (requires npm):
```
npm install -g serverless
```
Full docs can be found [here](https://www.serverless.com/framework/docs/providers/fn/guide/installation)

### Deploy
All resources can be deployed by running:
```
sls deploy
```

### Clean up
When done with the tests all the resources can be torn down with:
```
sls destroy
```
