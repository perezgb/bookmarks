#!/bin/bash

echo "Deploying stacks..."
aws cloudformation deploy --stack-name bookmarks-core --template stack.yaml --capabilities CAPABILITY_IAM  --region us-east-1
aws cloudformation deploy --stack-name bookmarks-api --template api-gateway.yaml  --region us-east-1
aws cloudformation deploy --stack-name bookmarks-firehose --template firehose.yaml --capabilities CAPABILITY_IAM  --region us-east-1
