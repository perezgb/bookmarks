#!/usr/bin/env python3

import boto3
from botocore.config import Config
from botocore.exceptions import ClientError
import json

config = Config(
    retries=dict(max_attempts=10), 
    region_name='us-east-1'
)

dynamodb = boto3.resource("dynamodb", config=config)
dynamodb_client = boto3.client("dynamodb", config=config)
table_name = "bookmark"
table = dynamodb.Table(table_name)


def create_table():
    """Create the table"""
    existing_tables = dynamodb_client.list_tables()["TableNames"]
    if table_name in existing_tables:
        print(f"Table {table_name} already exists.")
        return

    dynamodb_client.create_table(
        TableName=table_name,
        KeySchema=[
            {"AttributeName": "user", "KeyType": "HASH"},
            {"AttributeName": "href", "KeyType": "RANGE"},
        ],
        AttributeDefinitions=[
            {"AttributeName": "user", "AttributeType": "S"},
            {"AttributeName": "href", "AttributeType": "S"},
        ],
        ProvisionedThroughput={"ReadCapacityUnits": 5, "WriteCapacityUnits": 10},
    )

    print(f"Waiting for table {table_name}...")
    waiter = dynamodb_client.get_waiter("table_exists")
    waiter.wait(TableName=table_name)
    response = dynamodb_client.describe_table(TableName=table_name)
    print(response["Table"]["TableStatus"])


def put_item(item):
    try:
        print("Put item")
        tmp = {
            'user': 'perezgb',
            'href': item['href']
        }
        item['user'] = 'perezgb'
        if tmp['href']:
            table.put_item(Item=item)
    except ClientError as e:
        print(f'{e.response["Error"]["Code"]}: {e.response["Error"]["Message"]}')

def main():
    create_table()
    with open('../sample-data/pinboard-clean.json') as f:
        items = json.load(f)
        for item in items:
            put_item(item)

if __name__ == "__main__":
    main()