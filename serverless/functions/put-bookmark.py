import boto3
from botocore.config import Config
from botocore.exceptions import ClientError
import json

config = Config(
  retries=dict(max_attempts=10), 
  region_name='us-east-1'
)

dynamodb = boto3.resource('dynamodb', config=config)
dynamodb_client = boto3.client('dynamodb', config=config)
table_name = 'bookmark'
table = dynamodb.Table(table_name)

def lambda_handler(event, context):
  if 'tags' in event:
    event['tags'] = [tag.lower() for tag in event['tags']]
  if 'href' in event:
    event['href'] = event['href'].lower()
  table.put_item(Item=event)
  return {}