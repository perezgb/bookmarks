import json
import boto3

def get_tags(image):
  tags = image.get('tags', {}).get('L', [])
  return set([tag['S'] for tag in tags])

def get_item(client, user, tag):
  return client.get_item(
      TableName = 'tagStats',
      Key = { 'user': {'S': user}, 'tag': {'S': tag} }
  )

def put_item(client, item):
  client.put_item(
      TableName = 'tagStats',
      Item = item
  )

def update_item(client, user, tag, increment):
  return client.update_item(
              ExpressionAttributeValues={':val': {'N': str(increment)}},
              UpdateExpression='set cnt = cnt + :val',
              Key={ 'user': {'S': user}, 'tag': {'S': tag}  },
              TableName = 'tagStats',
              ReturnValues='UPDATED_NEW'
          )

def new_item(user, tag):
  return {
          'user': {'S': user},
          'tag': {'S': tag},
          'cnt': {'N': '1'}
      }

def increment(user, tag):
  print('incrementing user:', user, 'tag:', tag)
  ddbClient = boto3.client('dynamodb')

  currentStats = get_item(ddbClient, user, tag)
  print('found')

  if 'Item' not in currentStats:
      print('inserting')
      statItem = new_item(user, tag)
      put_item(ddbClient, statItem)
  else:
      print('updating')
      response = update_item(ddbClient, user, tag, increment=1)
      print("Increment succeeded:")
      print(json.dumps(response, indent=4))

def decrement(user, tag):
  print('decrementing user:', user, 'tag:', tag)
  ddbClient = boto3.client('dynamodb')

  currentStats = get_item(ddbClient, user, tag)
  print('found')

  if 'Item' not in currentStats:
      print('inserting')
      statItem = new_item(user, tag)
      put_item(ddbClient, statItem)
  else:
      print('updating')
      response = update_item(ddbClient, user, tag, increment=-1)
      print("Decrement succeeded:")
      print(json.dumps(response, indent=4))

def lambda_handler(event, context):
  for record in event['Records']:
      old_tags = set()
      new_tags = set()
      user = record['dynamodb']['Keys']['user']['S']

      if 'OldImage' in record['dynamodb']:
          old_tags = get_tags(record['dynamodb']['OldImage'])

      if 'NewImage' in record['dynamodb']:
          new_tags = get_tags(record['dynamodb']['NewImage'])

      tags_added = new_tags - old_tags
      tags_removed = old_tags - new_tags

      for tag in tags_removed:
          decrement(user, tag)

      for tag in tags_added:
          increment(user, tag)

  return {
      'statusCode': 200
  }